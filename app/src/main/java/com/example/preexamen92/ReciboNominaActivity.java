package com.example.preexamen92;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class ReciboNominaActivity extends AppCompatActivity {

    private EditText etNumRecibo, etNombre, etHorasNormales, etHorasExtras;
    private RadioGroup radioGroupPuesto;
    private TextView tvSubtotal, tvImpuesto, tvTotalAPagar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);

        etNumRecibo = findViewById(R.id.etNumRecibo);
        etNombre = findViewById(R.id.etNombre);
        etHorasNormales = findViewById(R.id.etHorasNormales);
        etHorasExtras = findViewById(R.id.etHorasExtras);
        radioGroupPuesto = findViewById(R.id.radioGroupPuesto);
        tvSubtotal = findViewById(R.id.tvSubtotal);
        tvImpuesto = findViewById(R.id.tvImpuesto);
        tvTotalAPagar = findViewById(R.id.tvTotalAPagar);
        Button btnCalcular = findViewById(R.id.btnCalcular);
        Button btnLimpiar = findViewById(R.id.btnLimpiar);
        Button btnRegresar = findViewById(R.id.btnRegresar);

        // Recibir datos del Intent
        String nombre = getIntent().getStringExtra("nombre");
        int numRecibo = generateFolioNumber();

        // Establecer los datos recibidos en los campos correspondientes
        etNombre.setText(nombre);
        etNumRecibo.setText(String.valueOf(numRecibo));
        etNumRecibo.setEnabled(false);
        etNombre.setEnabled(false);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numRecibo = Integer.parseInt(etNumRecibo.getText().toString());
                String nombre = etNombre.getText().toString();
                int horasNormales = Integer.parseInt(etHorasNormales.getText().toString());
                int horasExtras = Integer.parseInt(etHorasExtras.getText().toString());
                int puesto = getPuestoSeleccionado(radioGroupPuesto);
                double porcentajeImpuesto = 16.0;

                ReciboNomina recibo = new ReciboNomina(numRecibo, nombre, horasNormales, horasExtras, puesto, porcentajeImpuesto);
                double subtotal = recibo.calcularSubtotal();
                double impuesto = recibo.calcularImpuesto();
                double totalAPagar = recibo.calcularTotalAPagar();

                tvSubtotal.setText(String.valueOf(subtotal));
                tvImpuesto.setText(String.valueOf(impuesto));
                tvTotalAPagar.setText(String.valueOf(totalAPagar));
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etHorasNormales.setText("");
                etHorasExtras.setText("");
                radioGroupPuesto.clearCheck();
                tvSubtotal.setText("");
                tvImpuesto.setText("");
                tvTotalAPagar.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private int generateFolioNumber() {
        Random random = new Random();
        return random.nextInt(9000) + 1000; // Genera un número aleatorio de 4 dígitos
    }

    private int getPuestoSeleccionado(RadioGroup radioGroup) {
        int radioButtonID = radioGroup.getCheckedRadioButtonId();
        View radioButton = radioGroup.findViewById(radioButtonID);
        int idx = radioGroup.indexOfChild(radioButton);

        // Adjust index based on your position values
        // Assuming Auxiliar = 1, Albañil = 2, Ing. Obra = 3
        return idx + 1;
    }
}

